#!/usr/bin/env python3
#  @authors : CALUMINO <info@calumino.com>
#  @date    : June 2021 

import sys
import os
import argparse
import logging
import spidev
import time
import math
import numpy as np

from PIL      import Image
from gpiozero import LED, Button

### CONSTANTS ##################################################################

IR_NUM_COLS    = 38
IR_NUM_ROWS    = 17
IR_MDATA_ROWS  = 15
IR_IMG_SIZE    = (IR_NUM_COLS*(IR_NUM_ROWS+IR_MDATA_ROWS)*2)
OP_READ_FRAME  = 0xAF

CRC16_CCITT_TABLE = [
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0,
        ]



### CLASSES ####################################################################
                
class CTSRequestTimeoutError(Exception):
    """Class for CTS requesttimeout exceptions."""
    pass
    
    
class CTSRequestError(Exception):
    """Class for CTS request error exceptions."""
    pass
    
    
class CTS():
    
    def __init__(self,
                 bus=0, device=0, mode=0b11, freq=2000000, 
                 fr_rdy_gpio=16, nrst_gpio=19, csn_io=None,
                 fps=8):
        self.spi = spidev.SpiDev()
        self.bus = bus
        self.fr_rdy_gpio = fr_rdy_gpio
        try:
            self.spi.open(bus, device)
        except:
            raise Exception("Could not open SPI device BUS={} DEV={}.".format(bus, device))
        self.spi.mode         = mode
        self.spi.max_speed_hz = freq
        debug("CTS: initialized spidev{}.{} @{:4.2f}MHz, mode {}".format(bus, device, freq/1e6, mode))
        # Frame per seconds
        self.fps              = fps
        # GPIO: USER_FRRDY
        self.fr_rdy_btn       = Button(fr_rdy_gpio)
        self.nrst_io          = LED(nrst_gpio, initial_value=True)
        if csn_io is not None:
            self.csn_io       = LED(csn_io, initial_value=True)
        else:
            self.csn_io       = None
        
    # Private methods
    
    def __reverse_bits(self, byte):
        byte_inv = int('{:08b}'.format(byte)[::-1], 2)
        return byte_inv

    # Low-level commands
    
    def cmd_read_frame(self):
        data = [0x00] * (4 + IR_IMG_SIZE)
        data[0] = self.__reverse_bits(OP_READ_FRAME)
        if self.csn_io is not None:
            self.csn_io.off()
        time.sleep(0.01)
        resp = self.spi.xfer(data)
        time.sleep(0.01)
        if self.csn_io is not None:
            self.csn_io.on()
        return [self.__reverse_bits(x) for x in resp[4:]]
        
    # CTS control
    
    def reset(self):
        self.nrst_io.off()
        time.sleep(1)
        self.nrst_io.on()
        time.sleep(1)
        print("CTS Reset Complete")
    
    # Read IR frame
    
    def read_frame(self, timeout=1000):
        time_max = time.time() + timeout/1000
        while(time.time()<time_max):
            if self.fr_rdy_btn.is_pressed:
                byte_lst  = self.cmd_read_frame()
                pixel_lst = []
                for i in range(int(len(byte_lst)/2)):
                    cur_pix  = byte_lst[i*2+0]
                    cur_pix |= byte_lst[i*2+1]<<8
                    pixel_lst.append(cur_pix)
                return pixel_lst
            else:
                time.sleep(1/self.fps/4)
        raise CTSRequestTimeoutError()

### HELPERS ####################################################################

def error(s, e=None):
    logging.error(str(s))
    if e is not None:
        logging.error("%s" % (str(e)))
        
def warning(s, e=None):
    logging.warning(str(s))
    if e is not None:
        logging.warning("%s" % (str(e)))
        
def debug(s, e=None):
    logging.debug(str(s))
    if e is not None:
        logging.debug("%s" % (str(e)))
        
def info(s, e=None):
    logging.info(str(s))
    if e is not None:
        logging.info("%s" % (str(e)))
        
def crc16ccitt(data, crc=0):
    for byte in data:
        tbl_idx = ((crc >> 8) ^ byte) & 0xff;
        crc = (CRC16_CCITT_TABLE[tbl_idx] ^ (crc << 8)) & 0xffff;
    return crc & 0xffff;

def to_signed12(v):
    return (v^0x800) - 0x800

def load_mdata(data):
    mdata_dct = {}
    # serial_number
    mdata_dct["serial_number"] = data[0x00//2] + ((data[0x00//2+1]&0x00FF)<<16)
    # model_number
    mdata_dct["model_number"]  = data[0x04//2] + ((data[0x04//2+1]&0xFFFF)<<16)
    # fov_lens
    mdata_dct["fov_lens"]      = data[0x08//2] + ((data[0x08//2+1]&0xFFFF)<<16)
    # frame_per_sec
    mdata_dct["decentration"]  = data[0x0C//2] + ((data[0x0C//2+1]&0xFFFF)<<16)
    # decentration
    mdata_dct["calib_params"]  = data[0x10//2] + ((data[0x10//2+1]&0xFFFF)<<16)
    # temp_sensors
    mdata_dct["temp_sensors"]  = [None,None,None,None,None,None]
    for i in range (6):
        mdata_dct["temp_sensors"][i] = data[0x14//2+i]/256.
    # power_sensors
    mdata_dct["power_sensors"] = [None,None,None,None,None,None,None,None,None,None,None,None]
    for i in range (12):
        mdata_dct["power_sensors"][i] = data[0x24//2+i]
    # current_sensors
    mdata_dct["current_sensors"] = [None,None,None,None,None,None,None,None,None,None,None,None]
    for i in range (12):
        mdata_dct["current_sensors"][i] = data[0x3C//2+i]
    # voltage_sensors
    mdata_dct["voltage_sensors"] = [None,None,None,None,None,None,None,None,None,None,None,None]
    for i in range (12):
        mdata_dct["voltage_sensors"][i] = data[0x54//2+i]
    # bist_diag
    mdata_dct["bist_diag"]      = data[0x6C//2] + (data[0x6C//2+1]<<16)
    # uptime_sec
    mdata_dct["uptime_sec"]     = data[0x70//2] + (data[0x70//2+1]<<16)
    # frame_counter
    mdata_dct["frame_counter"]  = data[0x74//2] + (data[0x74//2+1]<<16)
    # accel
    mdata_dct["accel"] = [None, None,None]
    mdata_dct["accel"][0] = 0.012*to_signed12( ((data[0x78//2+0]&0xFFFF)>>4) )
    mdata_dct["accel"][1] = 0.012*to_signed12( ((data[0x78//2+1]&0xFFFF)>>4) )
    mdata_dct["accel"][2] = 0.012*to_signed12( ((data[0x7C//2+0]&0xFFFF)>>4) )
    # Software version
    mdata_dct["sw_version"]     = data[0x80//2] + (data[0x80//2+1]<<16)
    # bist_diag
    mdata_dct["exposure"]       = data[0x84//2] + (data[0x84//2+1]<<16)
    # # reserved
    # mdata_dct["reserved"]   = data[0x88//2] + (data[0x88//2+1]<<16)
    # led_lifetime
    mdata_dct["led_lifetime"]   = data[0x8C//2] + (data[0x8C//2+1]<<16)
    # crc16 ccitt
    mdata_dct["crc"]            = data[0xFE//2]
    return mdata_dct
    

### READ FRAME #################################################################

def show_ir_frame(cts, cv2):
    while True:
        # Get new frame
        try:
            cur_frame = cts.read_frame()
        except CTSRequestTimeoutError:
            error("Timeout while getting new IR frame.")
            return False
        # Display video?
        if cv2 is not None:
            # Divide by 256 since we can just display 8-bit value and the data is on 16-bit
            img = np.array(cur_frame) / 256
            # Reshape to 2D
            img        = np.reshape(img, (IR_NUM_COLS, IR_NUM_ROWS+IR_MDATA_ROWS))
            frame_data = img
            # Convert array to uint8
            img = np.uint8(img)
            # Convert to RGB (required for display)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            # Crop image to the actual pixel
            img = img[0:IR_NUM_ROWS, 0:IR_NUM_COLS]
            # Resize (so that we can actually see something)
            img = cv2.resize(img, (0,0), fx=10, fy=10, interpolation = cv2.INTER_NEAREST)
            # Display frame
            cv2.imshow('IR RAW', img)
            if cv2.waitKey(1) == 27:
                break  # esc to quit
        # Get metadata
        mdata = load_mdata(cur_frame[IR_NUM_ROWS*IR_NUM_COLS:])
        # Check CRC
        byte_lst = []
        for word in cur_frame:
            byte_lst.append((word&0x00FF))
            byte_lst.append((word&0xFF00)>>8)
        crc_extracted  = mdata["crc"]
        crc_computed   = crc16ccitt(byte_lst[0:IR_NUM_COLS*IR_NUM_ROWS*2+254], crc=0xFFFF)
        crc_is_correct = (crc_computed==crc_extracted)
        # Print data
        os.system('clear')
        str_out = \
            "device: /dev/spidev{cts.bus}.0\n" + \
            "spi mode: {cts.spi.mode}\n" + \
            "bits per word: {cts.spi.bits_per_word}\n" + \
            "max speed: {cts.spi.max_speed_hz} Hz\n" + \
            "frame ready GPIO: {cts.fr_rdy_gpio} (BCM GPIO notation)\n\n" + \
            "serial_number {m[serial_number]}\n" + \
            "hw_version TBD\n" + \
            "model_id {m[model_number]}\n" + \
            "sw_version {m[sw_version]:08X}\n" + \
            "FOV {m[fov_lens]}\n" + \
            "FPGA temp {m[temp_sensors][0]:6.3f}\n" + \
            "CMOS_ANALOG_T {m[temp_sensors][1]:6.3f}\n" + \
            "LED_J4_T {m[temp_sensors][2]:6.3f}\n" + \
            "DC_DC_T {m[temp_sensors][3]:6.3f}\n" + \
            "Flex_J7_T {m[temp_sensors][4]:6.3f}\n" + \
            "CMOS_DIGITAL_T {m[temp_sensors][5]:6.3f}\n" + \
            "uptime {m[uptime_sec]}\n" + \
            "frame_count {m[frame_counter]}\n" + \
            ""
        str_out = str_out.format(m=mdata, cts=cts)
        print(str_out)
    return    
    
    
### MAIN #######################################################################

def main():
    
    parser = argparse.ArgumentParser(description="Test CTS v1.1 SPI")
    parser.add_argument("-R","--reset", \
                        help="reset the CTS", \
                        action="store_true", default=False)
    parser.add_argument("-v","--verbose", \
                        help="make the script more verbose", \
                        action="store_true", default=False)
    parser.add_argument("-S","--show-ir-frame", \
                        help="read IR frame and display them", \
                        action="store_true",default=False)
    parser.add_argument("-V","--video", \
                        help="display video", \
                        action="store_true",default=False)
    parser.add_argument("--chipselect", \
                        help="Use a specific GPIO as chipselect", \
                        type=int,default=None)
                        
    args = parser.parse_args()
    
    ### VERBOSITY LEVEL ###
    if args.verbose==True:
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        
    ### SETUP CTS ###
    if args.chipselect is not None:
        my_cts = CTS(csn_io=args.chipselect)
    else:
        my_cts = CTS()
        
    ### RESET ###
    if args.reset==True:
        my_cts.reset()
        
    ### IR FRAME ###
    if args.show_ir_frame==True:
        if args.video==True:
            # Try importing OpenCV module
            try:
                import cv2
            except ImportError:
                error("Could not import cv2 module. Aborting.")
                sys.exit(-1)
        else:
            cv2 = None
        show_ir_frame(my_cts, cv2)


if __name__ == "__main__":
    logging.basicConfig(format='\033[1m%(asctime)-15s - %(levelname)s:\033[0m %(message)s', level=logging.INFO)
    main()
